<?php 
include './function.php';
/*
$arr = array(
	"input_1" => "a1",
	"name_1" => "b1",
	"input_2" => "a2",
	"name_2" => "b2",
	"radio_2" => "c2",
);
*/
function get_inputs($arr){
	$newarr = array();
	foreach ($arr as $key => $value) {
		 $t = explode("_",$key);
		 $newarr[$t[1]][$t[0]] = $value;
	}
	return $newarr;
}

function get_cookedtpl($tplpath, $arr){
	$str = file_get_contents($tplpath);
	return string_replace($str, $arr);
}

//todo handle $_POST for secerity
$arr = $_POST;
/*
$arr = array(
	"tplname_1"=>"input_checkbox.html",
	"title_1"=>"afsadf",
	"name_1"=>"adsf",
	"radioinfo_1"=>"adsf",
	"help_1"=>"adf",
	"tplname_1458291241024"=>"input_radio.html",
	"title_1458291241024"=>"afd",
	"name_1458291241024"=>"fasd",
	"radioinfo_1458291241024"=>"afd",
	"help_1458291241024"=>"afd"
	);
*/
$inputs_arr = get_inputs($arr);

foreach ($inputs_arr as $key => $value) {
	$tplpath = './tpl/'.$value["tplname"];
	//echo $tplpath;
	$t = get_cookedtpl($tplpath, $value);
	//echo $t;
	//echo "</pre>";
	$t = htmlspecialchars($t);
	//echo $t;
	//echo json_encode($t); // using json send structured data to fontend , but if the data is only string just echo !
	echo $t;
}
