<?php 
/**
 * 是否是AJAx提交的
 * @return bool
 */
function isAjax(){
  if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    return true;
  }else{
    return false;
  }
}

/**
 * 是否是GET提交的
 */
function isGet(){
  return $_SERVER['REQUEST_METHOD'] == 'GET' ? true : false;
}

/**
 * 是否是POST提交
 * @return int
 */
function isPost() {
  return ($_SERVER['REQUEST_METHOD'] == 'POST' && checkurlHash($GLOBALS['verify']) && (empty($_SERVER['HTTP_REFERER']) || preg_replace("~https?:\/\/([^\:\/]+).*~i", "\\1", $_SERVER['HTTP_REFERER']) == preg_replace("~([^\:]+).*~", "\\1", $_SERVER['HTTP_HOST']))) ? 1 : 0;
}

function string_replace($str, $reparr){
	foreach ($reparr as $key => $value) {
		$pattern = "/\[{$key}\]/i";
		$str = preg_replace($pattern, $value, $str);
	} 
	return $str;
}
// array('filename'=array('title', 'name'), )
function get_mark($filename){
	if(($str = file_get_contents($filename)) === FALSE){
		echo "file_get_contents error";
	}

	$pattern = "/\[([a-zA-Z_][a-zA-Z0-9_]*)\]/i";
	if(($ret = preg_match_all($pattern, $str, $matches)) == 0){
		echo "error match";
	} 
	return $matches[1];
}
//get_mark("./tpl/input_text.html");
function _readdir($dir){
	if(!is_dir($dir)){
		echo "not dir";
	}

	if (($dh = opendir($dir)) == FALSE) {
			echo "open failed";
	}

	$files_arr = array();
	while (($file = readdir($dh)) !== false) {
		if(filetype($dir.$file) == 'file'){
			$files_arr[] = $file;
		}
	}
	closedir($dh);
	return $files_arr;
}

